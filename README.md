# compressed-allocations

Utilities for generating the PulseChain allocations file.

*NOT FINAL NUMBERS*

## Step 1: generate raw data from sacrifice database

This data has already been exported and is included in [data/raw.csv](data/raw.csv)

## Step 2: [src-ts/main.ts](src-ts/main.ts)

Converts [data/raw.csv](data/raw.csv) into a simplified `ACCOUNT,CREDIT_BASE16,CREDIT_BASE10` formatted file. This file can be reviewed by the community.

```bash
yarn && yarn convert
```

a few optional flags can be passed to run non default projects through the pipeline such as a folder name (project) and a start time in iso format.

```bash
yarn convert --project=pulsex --start-time=2021-12-29T03:30:00Z
```

## Step 3: [src-go/compress/main.go](src-go/compress/main.go)

Small utility to compress the `ACCOUNT,CREDIT_BASE16,CREDIT_BASE10` file produced in Step 2 into a minimized binary format.

```bash
go run ./src-go/compress
```

> **The output [data/credits.bin](data/credits.bin) file is what will be embedded in the Go-Pulse binary**

## Auditing: [src-go/decompress/main.go](src-go/decompress/main.go)
A sample implementation of reading and parsing the minimized binary format produced in Step 2 that can be used to audit and verify the compressed binary data.

```bash
go run ./src-go/decompress
```

This allows for easy and direct review of the data in the binary credits file:

```bash
# example output
Reading 63 bytes from compressed file './data/credits.bin'
Addr: 0xceb59257450820132ab274ed61c49e5fd96e8868 | Credit: 0x33b2e3c9fd0803ce8000000 (1000000000.000000 PLS)
Addr: 0x731a5796c12638872ebe3f73e25576169ad9d87a | Credit: 0x3635c9adc5dea00000 (1000.000000 PLS)
```

## csv columns

- `mined_timestamp` - timestamp when the block was mined
- `transaction_hash` - transaction hash that the sacrifice is pulled from
- `credit_address_id` - an incrementing nonce for the frontend to be able to tie utxo networks back to an evm address
- `is_sens` - flag to distinguish sens from non-sens donations to reduce the points from sens
- `network` - network that the sacrifice was performed on
- `block_id` - block height that the sacrifice was mined in
- `currency` - currency identifier, often in the format of `network-hash`
- `ticker` - currency ticker for presentational purposes
- `decimals` - number of decimals in the currency
- `source` - source of the price, either from a dex, or cex, or coingecko, or derived from a sale
- `credit_address` - evm address to attribute points
- `advertised_for` - currency was advertised for on pulse.info
- `ignore` - currency has been flagged as untrustworthy and will not be given points with this flag
- `amount` - the value of the transfer as an int
- `usd_price` - usd price of the token twap'd where data provided

## generated files

- `credits.bin` - the compressed credits file to be used by the node
- `credits.csv` - the credits file, ordered by public address
- `credits.csv.example` - the example `credits.csv` file
- `point-events.csv` - a csv to show the points and how they are summed from their parts
- `raw-advertised-for.csv` - shows the transactions that fall under the advertised for category (must be included)
- `raw-ignored.csv` - transactions that will be ignored because they are scam tokens or other
- `raw-not-ignored.csv` - transactions that are being considered to be included in the final count
- `raw-reduced.csv` - a reduced version of `raw.csv` where only one row per transaction is used - the highest trustworthiness price source
- `raw.csv` - the raw transactions joined against price sources - often there will be duplicate rows in this csv because there are multiple price sources
