const DECIMALS_18_BASE = '000000000000000000';

export const DECIMALS_18_ONE = BigInt('1' + DECIMALS_18_BASE);

export function scaleToDecimals18(value: string): bigint {
  const d = value.indexOf('.');
  if (d < 0) {
    return BigInt(value + DECIMALS_18_BASE);
  }
  return BigInt(value.slice(0, d) + value.slice(d + 1, d + 1 + 18).padEnd(18, '0'));
}
