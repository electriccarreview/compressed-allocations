import { HASH_160_0X_LC, Rule } from './rules';
import { Address } from './types';

// As a sanity check
export const RAW_CSV_HEADER = 'mined_timestamp,transaction_hash,credit_address_id,is_sens,network,block_id,currency,ticker,decimals,source,credit_address,advertised_for,ignore,amount,usd_price';
const RAW_CSV_COL_NAMES = RAW_CSV_HEADER.split(',');

export function validateHeader(rawCsvPath: string, header?: string) {
  if (header === RAW_CSV_HEADER) {
    return;
  }

  if (!header) {
    throw new Error('Empty CSV file!');
  }
  const message = `
    Unexpected CSV header:

    ${header}

    Expected CSV header:

    ${RAW_CSV_HEADER}

    File: ${rawCsvPath}:1
`;
  throw new Error(message);
}

export function validateFieldCount(fieldCount: number) {
  if (fieldCount === RAW_CSV_COL_NAMES.length) {
    return;
  }

  const fieldInd = Math.min(fieldCount, RAW_CSV_COL_NAMES.length);
  throw new InvalidRowError(fieldInd, `${fieldCount} columns instead of ${RAW_CSV_COL_NAMES.length}`);
}

export function validateFieldType<T extends string>(fields: string[], fieldInd: number, choices: Set<T>): T {
  const s = fields[fieldInd] as T;

  if (choices.has(s)) {
    return s;
  }

  const colHeader = RAW_CSV_COL_NAMES[fieldInd];
  throw new InvalidRowError(fieldInd, `
    Unknown ${colHeader} type: '${s}'
`);
}

export function validateField(fields: string[], fieldInd: number, [re, desc]: Rule): string {
  const s = fields[fieldInd];

  if (re.test(s)) {
    return s;
  }

  throw new InvalidRowError(fieldInd, `
    Invalid value: '${s}'
    Expected:      ${desc}
`);
}

export function validateAddrRemap(addrRemap: Record<string, string>): Map<Address, Address> {
  const [re, desc] = HASH_160_0X_LC;

  const entries = Object.entries(addrRemap);

  entries
    .forEach((entry) => {
      entry.forEach((addr) => {
        if (!re.test(addr)) {
          const message = `ADDR_REMAP:
      Invalid value: '${addr}'
      Expected:      ${desc}
  `;
          throw new Error(message);
        }
      });
    });

  return new Map(entries);
}

export class InvalidRowError extends Error {
  fieldInd?: number;

  constructor(message: string);
  constructor(fieldInd: number, message: string);
  constructor(fieldIndOrMessage: number | string, message?: string) {
    let msg;
    let fieldInd;

    if (typeof fieldIndOrMessage === 'number') {
      fieldInd = fieldIndOrMessage;
      msg = message;
    } else {
      msg = fieldIndOrMessage;
    }

    super(msg);
    this.fieldInd = fieldInd;
  }

  addContext(path: string, row: string, rowInd: number) {
    const rowNum = rowInd + 1; // From 0-based to 1-based
    const lineNum = rowNum + 1; // Including header
    let file = `${path}:${lineNum}`;

    if (this.fieldInd !== undefined) {
      const pos = getRowFieldPos(row, this.fieldInd);
      if (pos !== 1) {
        file = `${file}:${pos}`;
      }

      if (this.fieldInd < RAW_CSV_COL_NAMES.length) {
        const colNum = this.fieldInd + 1;
        const colHeader = RAW_CSV_COL_NAMES[this.fieldInd];

        this.message +=
`
    Column:        #${colNum} (${colHeader})`;
      }
    }
    this.message +=
`
    Row:           #${rowNum}
    File:          ${file}

    Full row text:

    ${row}
`;
  }
}

function getRowFieldPos(row: string, fieldInd: number): number {
  let pos = 0;
  if (fieldInd > 0) {
    const fields = row.split(',');

    if (fieldInd < fields.length) {
      for (let i = 0; i < fieldInd; i++) {
        pos += fields[i].length + 1;
      }
    } else {
      pos = row.length;
    }
  }
  return pos + 1;
}
